import io.guanghuizeng.fs.FileSystem;
import io.guanghuizeng.fs.ServiceID;
import io.guanghuizeng.fs.Uri;
import io.guanghuizeng.fs.VirtualPath;
import io.guanghuizeng.mmdp.Engine;

import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by guanghuizeng on 16/5/25.
 */
public class TestExist {

    public static void main(String[] args) throws Exception {

        // 机器参数
        ServiceID service0 = new ServiceID("127.0.0.1", 8070, 8090); // (host | sync port | engine port)
        ServiceID service1 = new ServiceID("10.25.10.142", 8070, 8090);
		//        ServiceID service1 = new ServiceID("127.0.0.1", 8070, 8090); // TODO 修改

        // 测试数据的路径
        String input = "/data/data10m64";

        // 配置fs
        FileSystem fileSystem = new FileSystem();
        fileSystem.addServices(service0, service1);

        // 增加测试数据到 fs
        VirtualPath in = new VirtualPath(Paths.get(input));
        fileSystem.put(in, new Uri(service0, new VirtualPath(input)));
        fileSystem.put(in, new Uri(service1, new VirtualPath(input)));

        // 测试
        /** 生成检验数据 */
        Set<Long> testData = new HashSet<>();
        testData.add(1024L);
        testData.add(1054L);
        testData.add(24L);
        testData.add(100923L);
        testData.add(10243803L);
        testData.add(1003L);
        testData.add(10203L);
        testData.add(104303L);
        testData.add(102433L);
        testData.add(24380L);
        testData.add(1024803L);
        testData.add(243803L);

        /** 用 Engine.exist 方法判断存在性 */

        Engine engine = new Engine(fileSystem);
        double fpp = 0.03;

		System.out.println("Start...\n");        	   
		long startTime = System.currentTimeMillis();

		Map<Long, Boolean> existence = engine.exist(in, testData, fpp);

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);
        
        for (Map.Entry<Long, Boolean> e : existence.entrySet()) {
            System.out.printf("%d, %b\n", e.getKey(), e.getValue());
        }
    }
}
