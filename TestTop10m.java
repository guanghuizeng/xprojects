import io.guanghuizeng.fs.FileSystem;
import io.guanghuizeng.fs.ServiceID;
import io.guanghuizeng.fs.Uri;
import io.guanghuizeng.fs.VirtualPath;
import io.guanghuizeng.mmdp.Engine;

import java.nio.file.Paths;
import java.util.*;

/**
 * 测试Engine.top函数.
 * 注意事项:
 * - 生成对应数据
 * - 路径要匹配
 * - 机器配置要一致
 */
public class TestTop10m {

    public static void main(String[] args) throws Exception {

        // 机器参数
        ServiceID service0 = new ServiceID("127.0.0.1", 8070, 8090); // (host | sync port | engine port)
        ServiceID service1 = new ServiceID("10.25.10.142", 8070, 8090);
		//        ServiceID service1 = new ServiceID("127.0.0.1", 8070, 8090); // TODO 修改

        // 测试数据的路径
        String input = "/data/data10m64";

        // 配置fs
        FileSystem fileSystem = new FileSystem();
        fileSystem.addServices(service0, service1);

        // 增加测试数据到 fs
        VirtualPath in = new VirtualPath(Paths.get(input));
        fileSystem.put(in, new Uri(service0, new VirtualPath(input)));
        fileSystem.put(in, new Uri(service1, new VirtualPath(input)));

        // 初始化 Engine
        Engine engine = new Engine(fileSystem);

        // 测试
		System.out.println("Start...\n");        	   
		long startTime = System.currentTimeMillis();

		Map<Long, Long> result = engine.top(in, 10);

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);

        List<Map.Entry<Long, Long>> list = new LinkedList<>(result.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<Long, Long>>() {
            @Override
            public int compare(Map.Entry<Long, Long> o1, Map.Entry<Long, Long> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        for (Map.Entry<Long, Long> e : list) {
            System.out.println("Key: " + e.getKey() + " value: " + e.getValue());
        }
    }
}

