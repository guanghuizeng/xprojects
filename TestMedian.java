import io.guanghuizeng.fs.FileSystem;
import io.guanghuizeng.fs.ServiceID;
import io.guanghuizeng.fs.Uri;
import io.guanghuizeng.fs.VirtualPath;
import io.guanghuizeng.mmdp.Engine;

import java.nio.file.Paths;

/**
 * Created by guanghuizeng on 16/5/25.
 */
public class TestMedian {

    public static void main(String[] args) throws Exception {

        // 机器参数
        ServiceID service0 = new ServiceID("127.0.0.1", 8070, 8090); // (host | sync port | engine port)
        ServiceID service1 = new ServiceID("10.25.10.142", 8070, 8090);
		//        ServiceID service1 = new ServiceID("127.0.0.1", 8070, 8090); // TODO 修改

        // 测试数据的路径
        String input = "/data/data50m64";

        // 配置fs
        FileSystem fileSystem = new FileSystem();
        fileSystem.addServices(service0, service1);

        // 增加测试数据到 fs
        VirtualPath in = new VirtualPath(Paths.get(input));
        fileSystem.put(in, new Uri(service0, new VirtualPath(input)));
        fileSystem.put(in, new Uri(service1, new VirtualPath(input)));

        // 初始化 Engine
        Engine engine = new Engine(fileSystem);

        // 测试

		System.out.println("Start...\n");        	   
		long startTime = System.currentTimeMillis();

        int N = 50 * 1000 * 1000;  // 是数据个数
        Long result = engine.median(in, N);

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);

        // 输出结果
        System.out.printf("median: \n  input: %s\n  number: %d\n", input, result);

        System.out.printf("Done!\n");
    }
}
