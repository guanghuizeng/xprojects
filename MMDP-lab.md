# MMDP上机实验

### 环境准备

- 部署机器

  - 在阿里云购买机器3-5台，记录下机器信息

  - Ubuntu server 14.04

  - 增加用户，在 root 权限下执行，命令如下：

    `cmd: adduser <username>`

    `cmd: sudo adduser <username> sudo`

- 启动 ssh server，在本地用 ssh 连接 server

- 安装git

- 从BB获取配置脚本，然后执行

  - bb: https://guanghuizeng@bitbucket.org/guanghuizeng/config.git
  - 脚本分三个部分
    - 基本环境：安装 zsh、emacs、htop、screen等工具，并配置
    - Java开发环境：安装 jdk8、maven
      - 设置 java_home
    - mmdp环境：从 bb/git 获取项目代码；初始化文件系统
      - git: git clone https://github.com/guanghuizeng/MMDP.git   
      - bb: https://guanghuizeng@bitbucket.org/guanghuizeng/xprojects.git 

### 测试过程

- 初始化文件系统
  - 建立目录，将信息保存到Master节点
- 每台机器都生成数据
  - Script: RunGenData


- 启动各节点（包括master）
  - Scripts: RunSS, RunMS
- 开始测试（在Master）
  - Script: RunTest
  - 处理可能遇到的问题


- 记录结果

### 结束测试

- 关闭机器

