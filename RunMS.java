import io.guanghuizeng.mmdp.rpc.Server;

public class RunMS {

    public static void main(String[] args) throws Exception {

        int port = Integer.parseInt(args[0]);
        String home = args[1]; // "user1/" etc

        try {
            Server server = new Server(home);
            server.start(port);
        } finally {
            System.out.printf("Mmdp server closed.");
        }
    }
}

