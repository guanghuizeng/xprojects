import io.guanghuizeng.fs.sync.SyncServer;


public class RunSS {

    public static void main(String[] args) throws Exception {

        int port = Integer.parseInt(args[0]);
        String homepath = args[1];

        try {
            SyncServer syncServer = new SyncServer();
            syncServer.start(port, homepath);
        } finally {
            System.out.println("Server closed!");
        }
    }
}

