adduser gz

sudo adduser gz sudo

su gz

sudo apt-get update # 更新apt-get

sudo apt-get -y install git

cd & git clone https://guanghuizeng@bitbucket.org/guanghuizeng/config.git

cd config

sh ./config_basic.sh

sh ./config_java.sh

sh ./config_mmdp.sh



设置 maven 的仓库

```xml
<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 		    http://maven.apache.org/xsd/settings-1.1.0.xsd">
<mirrors>
<mirror>
  <id>CN</id>
  <name>OSChina Central</name>                                                                                                                       
  <url>http://maven.oschina.net/content/groups/public/</url>
  <mirrorOf>central</mirrorOf>
</mirror>
</mirrors>
</settings>
```


设置 locale

check which locales are supported:
`locale -a`
add the locales you want (for example ru):
`sudo locale-gen zh_CN`
`sudo locale-gen zh_CN.UTF-8`

`sudo update-locale`

ref: http://askubuntu.com/questions/76013/how-do-i-add-locale-to-ubuntu-server



设置文件系统

FileSystem的文件位置