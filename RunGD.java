import io.guanghuizeng.mmdp.utils.GenData;

import java.io.IOException;

/**
 * Created by guanghuizeng on 16/4/17.
 */
public class RunGD {

    public static void main(String[] args) throws IOException {

        String path = args[0]; // 路径
        int count = Integer.parseInt(args[1]); // 要生成的Long类型对象的数据量, 单位为m

        if (count > 1000) {
            System.out.println("Failed: too much!");
            return;
        }

        GenData.gen(path, 1000 * 1000 * count);
        System.out.println("Suecces:");
        System.out.printf("Count: %dm \n", count);
        System.out.printf("Size: %d MB\n", count * Long.BYTES);
        System.out.println("Path: " + System.getProperty("user.home").concat(path));
    }

}

