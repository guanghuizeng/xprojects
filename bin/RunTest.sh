#!/usr/bin/env bash

classname=$1
datasize=$2

java -classpath ".:$HOME/.m2/repository/io/netty/netty-all/4.1.0.CR7/netty-all-4.1.0.CR7.jar:$HOME/.m2/repository/junit/junit/4.11/junit-4.11.jar:$HOME/.m2/repository/org/hamcrest/hamcrest-core/1.3/hamcrest-core-1.3.jar:$HOME/.m2/repository/com/google/protobuf/protobuf-java/2.5.0/protobuf-java-2.5.0.jar:$HOME/.m2/repository/com/google/guava/guava/19.0/guava-19.0.jar:$HOME/.m2/repository/com/jcraft/jzlib/1.1.2/jzlib-1.1.2.jar:$HOME/.m2/repository/io/guanghuizeng/mmdp/1.0-SNAPSHOT/mmdp-1.0-SNAPSHOT.jar" "Test$classname$datasize"
