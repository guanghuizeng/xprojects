#!/usr/bin/env bash

sh ./compile.sh ../RunGD.java
sh ./compile.sh ../RunMS.java
sh ./compile.sh ../RunSS.java

sh ./compile.sh ../TestExist.java
sh ./compile.sh ../TestMax.java
sh ./compile.sh ../TestMedian.java
sh ./compile.sh ../TestSort.java
sh ./compile.sh ../TestTop.java
