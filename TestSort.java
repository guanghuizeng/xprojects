import io.guanghuizeng.fs.FileSystem;
import io.guanghuizeng.fs.ServiceID;
import io.guanghuizeng.fs.Uri;
import io.guanghuizeng.fs.VirtualPath;
import io.guanghuizeng.mmdp.Engine;

import java.nio.file.Paths;

/**
 * Created by guanghuizeng on 16/5/25.
 */
public class TestSort {

    public static void main(String[] args) throws Exception {

        // 机器参数
        ServiceID service0 = new ServiceID("127.0.0.1", 8070, 8090); // (host | sync port | engine port)
        ServiceID service1 = new ServiceID("10.25.10.142", 8070, 8090);
		//        ServiceID service1 = new ServiceID("127.0.0.1", 8070, 8090); // TODO 修改

        // 测试数据的路径
		//        String input = "/data/data10m64";
		//        String output = "/data/data10m64Sorted";

        String input = "/data/data50m64";
        String output = "/data/data50m64Sorted";

        // 配置fs
        FileSystem fileSystem = new FileSystem();
        fileSystem.addServices(service0, service1);

        // 增加测试数据到 fs
        VirtualPath in = new VirtualPath(Paths.get(input));
        fileSystem.put(in, new Uri(service0, new VirtualPath(input)));
        fileSystem.put(in, new Uri(service1, new VirtualPath(input)));

        VirtualPath out = new VirtualPath(Paths.get(input));

        // 初始化 Engine
        Engine engine = new Engine(fileSystem);

        // 测试
		System.out.println("Start...\n");        	   
		long startTime = System.currentTimeMillis();

        engine.sort(in,out);

		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;
		System.out.println(elapsedTime);

        System.out.printf("Done!\n");
    }
}
